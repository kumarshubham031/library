const express = require('express');
const router = express.Router();
const baseString = 'section';
const Sections = require('./controller');

// Views
router.get(`/${baseString}/sections-list`, Sections.sectionsView);

// Routes
router.get(`/${baseString}`, Sections.fetchAll);
router.get(`/${baseString}/:number`, Sections.aSection);
router.post(`/${baseString}`, Sections.addSection);
router.put(`/${baseString}/:id`, Sections.updateSection);
router.delete(`/${baseString}/:id`, Sections.removeSection);

module.exports = router;