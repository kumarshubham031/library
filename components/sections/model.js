const { Schema, model } = require('mongoose');
const SectionsSchema = new Schema({
    section: { type: String, required: true, unique: true },
},
{ timestamps: true },
);

const Sections = model('sections', SectionsSchema);
module.exports = Sections;
