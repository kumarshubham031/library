const Sections = require('./model');
const { ObjectId } = require('mongodb');

const fetchAll = async (req, res) => {
    try {
        const query = {};
        if (req.query.search) {
            query.section = {
                $regex: `.*${req.query.search.trim()}*.`,
                $options: 'i',
            }
        }
        console.log(query);
        const allSections = await Sections.find(query);
        const count = await Sections.find().countDocuments(query);
        const response = {
            Sections: allSections,
            total: count,
        }
        return res.status(200).json({ response, message: 'All Sections Found' });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const addSection = async (req, res) => {
    try {
        const data = req.body;
        const resp = await Sections(data).save();
        const responseData = {
            section: resp,
        }
        return res.status(200).json({ message: 'success', responseData });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const updateSection = async (req, res) => {
    try {
        const data = req.body;
        const resp = await Sections.updateOne({ _id: new ObjectId(req.params.id) }, { $set: data });
        return res.status(200).json({ message: 'success', data, resp });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const removeSection = async (req, res) => {
    try {
        await Sections.findByIdAndDelete(req.params.id);
        return res.status(200).json({ message: 'success' });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const modifySection = async (req, res) => {
    try {
        const data = req.body;
        console.log(req.body);
        return res.status(200).json({ message: 'success', data });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const aSection = async (req, res) => {
    try {
        // const number = base64encode(req.params.number);
        const number = req.params.number;
        const allSections = await Sections.findOne({ number });
        const response = {
            section: allSections,
        }
        return res.status(200).json({ message: 'success', response });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const sectionsView = async (req, res) => {
    try {
        const allSections = await Sections.find();
        const count = await Sections.find().countDocuments();
        const response = {
            Sections: allSections,
            total: count,
        }
        res.render('sections', response);
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

module.exports = {
    fetchAll,
    addSection,
    removeSection,
    modifySection,
    updateSection,
    aSection,
    sectionsView,
}