const Classes = require('./classes/route');
const Sections = require('./sections/route');
const Subjects = require('./subjects/route');
const Users = require('./users/route');
const Books = require('./books/route');

module.exports = [
    Classes,
    Sections,
    Subjects,
    Users,
    Books,
];