const Joi = require('joi');

const addBook = Joi.object({
    name: Joi.string().required().messages({ 'any.required': 'Please enter book name' }),
    price: Joi.number().required().messages({ 'any.required': 'Please enter price' }),
    author: Joi.string().required().messages({ 'any.required': 'Please enter author name' }),
    image: Joi.string(),
    year: Joi.number().required().messages({ 'any.required': 'Please enter book year' }),
    subject: Joi.array(), // .required().messages({ 'any.required': 'Please enter book subject' }),
    class: Joi.array(), // .required().messages({ 'any.required': 'Please enter login id' }),
    available: Joi.boolean(),
});

module.exports = {
    addBook,
}
