const Books = require('./model').Books;
const Validate = require('./validate');
const response = require('../../common/response');
const { ObjectId } = require('mongodb');

const fetchAll = async (req, res) => {
    try {
        const query = {};
        const page = 1;
        const limit = req.query.limit ? parseInt(req.query.limit, 10) : 10;
        const offset =  req.query.offset ? parseInt(req.query.offset, 10) : limit * (page - 1);

        if (req.query.search) {
            query.name = {
                $regex: `.*${req.query.search.trim()}*.`,
                $options: 'i',
            }
        }
        if (req.query.available) {
            query.available = req.query.available;
        }
        if (req.query.author) {
            query.author = {
                $regex: `.*${req.query.author.trim()}*.`,
                $options: 'i',
            }
        }
        const allBooks = await Books.find(query).skip(offset).limit(limit);
        const responseData = {
            Books: allBooks,
        }
        return response.success(res, responseData, 'All Books Found');
    } catch (error) {
        return response.failure1(res, error);
    }
}

const addBook = async (req, res) => {
    try {
        const data = req.body;
        const validateData = await Validate.addBook.validate(data);
        if (validateData && validateData.error) { return res.status(400).json({ message: validateData.error.details[0].message }); }

        const savedBook = await Books(data).save();
        const responseData = {
            book: savedBook,
        }
        return response.success(res, responseData, 'Book added successfully');
    } catch (error) {
        return response.failure1(res, error);
    }
}

const updateBook = async (req, res) => {
    try {
        const data = req.body;
        console.log(req.params.book_id);
        const resp = await Books.updateOne({ _id: new ObjectId(req.params.book_id) }, { $set: data });
        return res.status(200).json({ message: 'success', data });
    } catch (error) {
        return response.failure1(res, error);
    }
}

// const removeBook = async (req, res) => {
//     try {
//         const data = req.body;
//         console.log(req.body);
//         return res.status(200).json({ message: 'success', data });
//     } catch (error) {
//         return res.status(400).json({ message: 'error occurred', error });
//     }
// }

// const modifyBook = async (req, res) => {
//     try {
//         const data = req.body;
//         console.log(req.body);
//         return res.status(200).json({ message: 'success', data });
//     } catch (error) {
//         return res.status(400).json({ message: 'error occurred', error });
//     }
// }

// const ABook = async (req, res) => {
//     try {
//         // const number = base64encode(req.params.number);
//         const number = req.params.number;
//         const allBooks = await Books.findOne({ number });
//         const response = {
//             Book: allBooks,
//         }
//         return res.status(200).json({ message: 'success', response });
//     } catch (error) {
//         return res.status(400).json({ message: 'error occurred', error: error.message });
//     }
// }

module.exports = {
    fetchAll,
    addBook,
    updateBook,
    // removeBook,
    // modifyBook,
    // ABook,
}