const { Schema, model } = require('mongoose');
const booksSchema = new Schema({
    name: { type: String, lowercase: true, required: true },
    price: { type: Number, required: true },
    author: { type: String, lowercase: true, required: true },
    image: { type: Array, required: false },
    year: { type: Number, required: true },
    subject: [
        { type: Schema.Types.ObjectId, ref: 'subjects' }
    ],
    class: { type: Array, required: true },
    available: { type: Boolean, default: false },
    is_borrowed: { type: Boolean, default: false },
},
{ timestamps: true },
);

const Books = model('books', booksSchema);
module.exports = {
    Books,
}
