const express = require('express');
const router = express.Router();
const baseString = 'book';
const Books = require('./controller');

router.get(`/${baseString}`, Books.fetchAll);
// router.get(`/${baseString}/:number`, Books.AClass);
router.post(`/${baseString}`, Books.addBook);
router.put(`/${baseString}/:book_id`, Books.updateBook);
// router.delete(`/${baseString}`, Books.removeClass);
// router.patch(`/${baseString}`, Books.modifyClass);

module.exports = router;