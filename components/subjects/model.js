const { Schema, model } = require('mongoose');
const SubjectsSchema = new Schema({
    subject: { type: String, required: true, unique: true },
},
{ timestamps: true },
);

const Subjects = model('Subjects', SubjectsSchema);
module.exports = Subjects;
