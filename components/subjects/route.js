const express = require('express');
const router = express.Router();
const baseString = 'subjects';
const Subjects = require('./controller');

// Views
router.get(`/${baseString}/subjects-list`, Subjects.subjectsView);

// API's 
router.get(`/${baseString}`, Subjects.fetchAll);
router.get(`/${baseString}/:number`, Subjects.aSubject);
router.post(`/${baseString}`, Subjects.addSubject);
router.put(`/${baseString}/:id`, Subjects.updateSubject);
router.delete(`/${baseString}/:id`, Subjects.removeSubject);

module.exports = router;