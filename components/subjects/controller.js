const Subjects = require('./model');
const { ObjectId } = require('mongodb');

const fetchAll = async (req, res) => {
    try {
        const query = {};
        if (req.query.search) {
            query.Subject = {
                $regex: `.*${req.query.search.trim()}*.`,
                $options: 'i',
            }
        }
        console.log(query);
        const allSubjects = await Subjects.find(query);
        const count = await Subjects.find().countDocuments(query);
        const response = {
            Subjects: allSubjects,
            total: count,
        }
        return res.status(200).json({ response, message: 'All Subjects Found' });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const addSubject = async (req, res) => {
    try {
        const data = req.body;
        const resp = await Subjects(data).save();
        const responseData = {
            Subject: resp,
        }
        return res.status(200).json({ message: 'success', responseData });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const updateSubject = async (req, res) => {
    try {
        const data = req.body;
        const resp = await Subjects.updateOne({ _id: new ObjectId(req.params.id) }, { $set: data });
        return res.status(200).json({ message: 'success', data, resp });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const removeSubject = async (req, res) => {
    try {
        await Subjects.findByIdAndDelete(req.params.id);
        return res.status(200).json({ message: 'success' });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const modifySubject = async (req, res) => {
    try {
        const data = req.body;
        console.log(req.body);
        return res.status(200).json({ message: 'success', data });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const aSubject = async (req, res) => {
    try {
        // const number = base64encode(req.params.number);
        const number = req.params.number;
        const allSubjects = await Subjects.findOne({ number });
        const response = {
            Subject: allSubjects,
        }
        return res.status(200).json({ message: 'success', response });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const subjectsView = async (req, res) => {
    try {
        const allSubjects = await Subjects.find();
        const count = await Subjects.find().countDocuments();
        const response = {
            Subjects: allSubjects,
            total: count,
        }
        res.render('subjects', response);
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

module.exports = {
    fetchAll,
    addSubject,
    removeSubject,
    modifySubject,
    updateSubject,
    aSubject,
    subjectsView,
}