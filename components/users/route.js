const express = require('express');
const router = express.Router();
const baseString = 'users';
const users = require('./controller');
const auth = require('../../common/token');
const Uploads = require('../../common/upload');
const rolePermission = ['student', 'librarian'];
// Views
router.get(`/${baseString}/users-list`, users.usersView);

// Routes
router.get(`/${baseString}`, users.fetchAll);
router.get(`/${baseString}/list`, [auth.roleCheck(['librarian'])], users.fetchAllStudents);
router.post(`/${baseString}`, users.addUsers);
router.post(`/${baseString}/borrow`, [auth.decodeToken, auth.roleCheck(['librarian'])], users.borrow);
router.put(`/${baseString}/return-books`, [auth.roleCheck(['librarian'])], users.returnBooks);
router.post(`/${baseString}/login`, users.login);
router.get(`/${baseString}/login-user-info`, [auth.decodeToken, auth.roleCheck(rolePermission)] , users.loggedInUserInfo);
router.get(`/${baseString}/logout`, users.logout);
router.get(`/${baseString}/profile-upload`, Uploads.upload.single('name'), users.profileUpload);
router.get(`/${baseString}/two-profile-upload`, Uploads.upload.array('name', 2), users.profileUpload);
router.get(`/${baseString}/multiple-profile-upload`, Uploads.upload.fields([{ name: 'photo1', maxCount: 2 }, { name: 'file2', maxCount: 2 }] ), users.profileUpload);
router.put(`/${baseString}/update-profile`, [Uploads.upload.single('profile_pic'), auth.roleCheck(rolePermission)], users.updateProfile);
router.put(`/${baseString}/set-password/:user_id`, users.setPassword);
router.get(`/${baseString}/:number`, users.aUsers);
router.put(`/${baseString}/:id`, users.updateUsers);

router.delete(`/${baseString}/:id`, users.removeUsers);

module.exports = router;