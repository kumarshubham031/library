const Joi = require('joi');

const login = Joi.object({
    login_id: Joi.string().required().messages({ 'any.required': 'Please enter login id' }),
    password: Joi.string().required().messages({ 'any.required': 'Please enter password' }),
});

const register = Joi.object({
    first_name: Joi.string().required().messages({ 'any.required': 'Please enter first name' }),
    last_name: Joi.string().required().messages({ 'any.required': 'Please enter last name' }),
    gender: Joi.string().required().valid('Male', 'Female').messages({ 'any.required': 'Please enter gender' }),
    date_of_birth: Joi.date(), //.required().messages({ 'any.required': 'Please enter date of birth' }),
    phone_number: Joi.number().max(10).min(10), // .required().messages({ 'any.required': 'Please enter phone number' }),
    password: Joi.string().required().messages({ 'any.required': 'Please enter password' }),
    login_id: Joi.string(), // .required().messages({ 'any.required': 'Please enter login id' }),
    type: Joi.string(),
});

const modify = Joi.object({
    first_name: Joi.string(),
    last_name: Joi.string(),
    gender: Joi.string().valid('Male', 'Female'),
    date_of_birth: Joi.date(),
    phone_number: Joi.number().min(10),
    profile_pic: Joi.string(),
    class_number: Joi.string(),
    section: Joi.string(),
});

const borrow = Joi.object({
    book_id: Joi.array().required().messages({ 'any.required': 'Please enter book id' }),
    user_id: Joi.string().required().messages({ 'any.required': 'Please enter user id' }),
});

module.exports = {
    login,
    register,
    borrow,
    modify,
}
