const { Schema, model } = require('mongoose');
const bcrypt = require('bcrypt');

const UsersSchema = new Schema({
    first_name: { type: String, required: false, trim: true },
    last_name: { type: String, required: false, trim: true },
    class_number: { type: Schema.Types.ObjectId, required: false, ref: 'classes' },
    section: { type: String, required: false, ref: 'sections' },
    year: { type: String, required: false, trim: true },
    gender: { type: String, required: false, trim: true },
    date_of_birth: { type: Date, required: false, trim: true },
    login_id: { type: String, required: false, trim: true },
    password: { type: String, required: false, trim: true },
    profile_pic: { type: String, required: false, trim: true },
    phone_number: { type: Number, required: false, trim: true },
    type: { type: String, required: false, trim: true, default: 'student' },
},
{ timestamps: true },
);

const CounterSchema = new Schema({
    _id: { type: String, required: false },
    seq: { type: Number, default: 0 },
});

const Counter = model('Counter', CounterSchema);

UsersSchema.pre('save', async function save(next) {
    const doc = this;
    try {
        // ----------- Counter Code ------------------ //
        const counter = await Counter.findByIdAndUpdate(
            { _id: 'userId' },
            { $inc: { seq: 1 } },
            { new: true, upsert: true },
        );
        doc.login_id = `LIB000${counter.seq}`;
        // ----------- Counter Code ------------------ //

        // ----------- Password Hashing Code ------------------ //
        if (this.isModified('password')) {
            const salt = await bcrypt.genSalt(10);
            this.password = await bcrypt.hash(this.password, salt);
        }
        // ----------- Password Hashing Code ------------------ //

        next();
    } catch (error) {
        return next(error);
    }
});

const BlackListTokenSchema = new Schema({
    token: { type: String, required: true },
    status: { type: String, default: 'invalid' },
});

const BorrowSchema = new Schema({
    book_id: { type: Schema.Types.ObjectId, required: true, ref: 'Books' },
    user_id: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
    return_on: { type: Date, required: true },
    returning_date: { type: Date, required: false },
},
{ timestamps: true },
);

const Users = model('Users', UsersSchema);
const Blacklist = model('Blacklists', BlackListTokenSchema);
const Borrows = model('Borrows', BorrowSchema);

module.exports = {
    Users,
    Counter,
    Blacklist,
    Borrows,
};
