const Users = require('./model').Users;
const Borrows = require('./model').Borrows;
const Books = require('../books/model').Books;
const Blacklist = require('./model').Blacklist;
const mail = require('../../common/mail').sendMail;
const Validate = require('./validate');
const Token = require('../../common/token');
const fileUpload = require('../../common/upload');
const response = require('../../common/response');
const { ObjectId } = require('mongodb');
const bcrypt = require('bcrypt');
const jwt = require('jwt-simple');
const maxIssueAllowed = 3;
const returnAfterWeeks = 3;

const fetchAll = async (req, res) => {
    try {
        const query = {};
        if (req.query.search) {
            query.Users = {
                $regex: `.*${req.query.search.trim()}*.`,
                $options: 'i',
            }
        }
        const allUsers = await Users.find(query);
        const count = await Users.find().countDocuments(query);
        const response = {
            Users: allUsers,
            total: count,
        }
        return res.status(200).json({ response, message: 'All Users Found' });
    } catch (error) {
        console.log(error.stack);
        return response.failure(res, error.message);
    }
}

const fetchAllStudents = async (req, res) => {
    try {
        const query = { type: 'student' };
        if (req.query.search) {
            query.Users = {
                $regex: `.*${req.query.search.trim()}*.`,
                $options: 'i',
            }
        }
        const allUsers = await Users.find(query);
        const count = await Users.find().countDocuments(query);
        const response = {
            Users: allUsers,
            total: count,
        }
        return res.status(200).json({ response, message: 'All Students Found' });
    } catch (error) {
        console.log(error.stack);
        return response.failure(res, error.message);
    }
}

const addUsers = async (req, res) => {
    try {
        const data = req.body;
        const validateData = await Validate.register.validate(data);
        if (validateData && validateData.error) { return res.status(400).json({ message: validateData.error.details[0].message }); }

        const resp = await Users(data).save();
        const responseData = {
            Users: resp,
        }
        const encodedToken = Token.createToken({ login_id: data.login_id });
        await setTokenInHeaders(encodedToken, res);
        return res.status(200).json({ message: 'success', responseData });
        // return response.success(res, response, 'logged out successfully');
    } catch (error) {
        console.log(error);
        return response.failure(res, error.message);
    }
}

const updateUsers = async (req, res) => {
    try {
        const data = req.body;
        const validateData = await Validate.register.validate(data);
        if (validateData && validateData.error) { return res.status(400).json({ message: validateData.error.details[0].message }); }

        await Users.updateOne({ _id: new ObjectId(req.params.id) }, { $set: data });
        const responseData = {
            data,
        }
        return response.success(res, responseData, 'Profile updated successfully');
    } catch (error) {
        console.log(error.stack);
        return response.failure(res, error.message);
    }
}

const updateProfile = async (req, res) => {
    try {
        const data = req.body;
        // console.log(req.file);
        if (req.file) {
            data.profile_pic = 'uploads/' + req.file.filename;
        }
        const validateData = await Validate.modify.validate(data);
        if (validateData && validateData.error) { return res.status(400).json({ message: validateData.error.details[0].message }); }

        await Users.updateOne({ login_id: res.token_info.login_id }, { $set: data });
        const responseData = {
            key: 'value',
            data,
        }
        return response.success(res, responseData, 'Profile updated successfully');
    } catch (error) {
        console.log(error.stack);
        return response.failure(res, error.message);
    }
}

const removeUsers = async (req, res) => {
    try {
        await Users.findByIdAndDelete(req.params.id);
        return res.status(200).json({ message: 'success' });
        // return response.success(res, response, 'logged out successfully');
    } catch (error) {
        console.log(error.stack);
        return response.failure(res, error.message);
    }
}

const modifyUsers = async (req, res) => {
    try {
        const data = req.body;
        console.log(req.body);
        return res.status(200).json({ message: 'success', data });
        // return response.success(res, response, 'logged out successfully');
    } catch (error) {
        return response.failure(res, error.message);
    }
}

const aUsers = async (req, res) => {
    try {
        // const number = base64encode(req.params.number);
        const number = req.params.number;
        const allUsers = await Users.findOne({ number });
        const response = {
            Users: allUsers,
        }
        return res.status(200).json({ message: 'success', response });
        // return response.success(res, response, 'logged out successfully');
    } catch (error) {
        return response.failure(res, error.message);
    }
}

const setTokenInHeaders = async (encodedToken, res) => {
    try {
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Authorization', `Bearer ${encodedToken}`);
        res.setHeader('Access-Control-Expose-Headers', ['Authorization', 'x-amzn-Remapped-authorization']);
    } catch (error) {
        return response.failure(res, error.message);
    }
}

const decodeToken = async (req, res) => {
    try {
        return response.success(res, { decoded_token: res.decodedData }, 'Token decoded Successfully');
    } catch (error) {
        console.log(error.stack);
        return response.failure(res, error.message);
    }
}

const login = async (req, res) => {
    try {
        // await mail({ key: 'value' });
        const data = req.body;
        const validateData = await Validate.login.validate(data);
        if (validateData && validateData.error) { return res.status(400).json({ message: validateData.error.details[0].message }); }

        const userData = await Users.findOne({ login_id: data.login_id });
        if (!userData) {
            return res.status(400).json({ message: 'User not exist' });
        }
        const verifyPassword = await bcrypt.compare(data.password, userData.password);
        if (verifyPassword === false) {
            return res.status(400).json({ message: `Sorry Password didn't Matched` });
        }
        const responseData = {
            user: userData,
        }
        const tokenObj = { login_id: data.login_id, role: userData.type };
        if (userData.first_name) {
            tokenObj.name = userData.first_name;
        }
        if (userData.last_name) {
            tokenObj.name = userData.last_name;
        }
        if (userData.first_name && userData.last_name) {
            tokenObj.name = userData.first_name + ' ' + userData.last_name;
        }
        const encodedToken = Token.createToken(tokenObj);
        await setTokenInHeaders(encodedToken, res);
        return response.success(res, responseData, 'Login Successfully');
    } catch (error) {
        const obj = { message: 'error occurred', error: error.message };
        if (error.stack) {
            console.log(error.stack);
            obj.error_at_line = error.stack;
        }
        return res.status(400).json(obj);
    }
}

const loggedInUserInfo = async (req, res) => {
    try {
        const allUsers = await Users.findOne({ login_id: res.decodedData.login_id }).select('-password');
        const responseData = {
            Users: allUsers,
            decodedData: res.decodedData,
        }
        return response.success(res, responseData, 'success');
    } catch (error) {
        return response.failure1(res, error);
    }
}

const logout = async (req, res) => {
    try {
        await Blacklist({ token: (req.headers.authorization).split(' ')[1] }).save();
        const response = {}
        return response.success(res, response, 'logged out successfully');
    } catch (error) {
        return response.failure(res, error.message);
    }
}

const profileUpload = async (req, res) => {
    try {
        const data = req.body;
        const files = req.file;
        // console.log(files);
        // await fileUpload.uploadSingle(req, res);
        const response = {}
        return response.success(res, response, 'file uploaded successfully');
    } catch (error) {
        return response.failure(res, error.message);
    }
}

const borrow = async (req, res) => {
    try {
        const data = req.body;
        const validateData = await Validate.borrow.validate(data);
        if (validateData && validateData.error) { return res.status(400).json({ message: validateData.error.details[0].message }); }

        const checkPreviousBorrowedBooks = await Borrows.findOne({ user_id: data.user_id, returning_date: { $exists: false } }).countDocuments();
        if (checkPreviousBorrowedBooks === maxIssueAllowed) {
            return response.failure(res, 'Max ' + maxIssueAllowed + ' books can be assigned');
        }
        const query = { available: true, _id: { $in: data.book_id }, is_borrowed: false };
        const allBooks = await Books.find(query);
        if (allBooks.length !== data.book_id.length) {
            return response.failure(res, 'All books are not available');
        }
        if ((checkPreviousBorrowedBooks + allBooks.length) >= maxIssueAllowed) {
            return response.failure(res, 'Borrow limit is exceeding');
        }
        const arr = [];
        const today = new Date();
        const returnOn = new Date(today.setDate(today.getDate() + (returnAfterWeeks * 7) ));
        for (let i = 0; i < data.book_id.length; i += 1) {
            arr.push({
                book_id: new ObjectId(data.book_id[i]),
                user_id: new ObjectId(data.user_id),
                return_on: returnOn,
            });
        }
        await Borrows.insertMany(arr);
        await Books.updateMany({ _id: { $in: data.book_id } }, { $set: { is_borrowed: true } });
        const responseData = {

        };
        return response.success(res, responseData, 'Book Borrowed successfully');
    } catch (error) {
        return response.failure1(res, error);
    }
}

const usersView = async (req, res) => {
    try {
        const allUsers = await Users.find();
        const count = await Users.find().countDocuments();
        const response = {
            Users: allUsers,
            total: count,
        }
        res.render('users', response);
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const setPassword = async (req, res) => {
    try {
        if (!req.body.password) {
            return response.failure(res, 'Please enter password');
        } else if (req.body.password && (req.body.password.length > 10 || req.body.password.length < 5) ) {
            return response.failure(res, 'Password must be between 5 to 10 characters');
        }
        const user = await Users.findOne({ _id: req.params.user_id, type: 'student' });
        if (!user) {
            return response.failure(res, 'User not exists');
        } else if (user.password) {
            return response.failure(res, 'Password already set');
        }
        // !
        const salt = await bcrypt.genSalt(10);
        const password = await bcrypt.hash(req.body.password, salt);
        // !
        await Users.updateOne({ _id: new ObjectId(req.params.user_id) }, { $set: { password } });
        const responseData = {}
        return response.success(res, responseData, 'Password set successfully');
    } catch (error) {
        return response.failure1(res, error);
    }
}

const returnBooks = async (req, res) => {
    try {
        const data = req.body;
        const validateData = await Validate.borrow.validate(data);
        if (validateData && validateData.error) { return res.status(400).json({ message: validateData.error.details[0].message }); }

        const checkBorrowedBooks = await Borrows.find({ user_id: data.user_id, returning_date: { $exists: false }, book_id: { $in: data.book_id } }).countDocuments();
        if (checkBorrowedBooks !== data.book_id.length) {
            return response.failure(res, 'All Books not found');
        }
        await Borrows.updateMany({ user_id: data.user_id, returning_date: { $exists: false }, book_id: { $in: data.book_id } }, { $set: { returning_date: new Date() } })
        await Books.updateMany({ is_borrowed: true, _id: { $in: data.book_id } }, { $set: { is_borrowed: false } });
        const responseData = {}
        return response.success(res, responseData, 'Book returned successfully');
    } catch (error) {
        return response.failure1(res, error);
    }
}

module.exports = {
    fetchAll,
    addUsers,
    removeUsers,
    modifyUsers,
    updateUsers,
    aUsers,
    login,
    loggedInUserInfo,
    logout,
    profileUpload,
    borrow,
    usersView,
    updateProfile,
    fetchAllStudents,
    setPassword,
    returnBooks,
}