const http = require('http');
const fs = require('fs');
const Classes = require('./model');
const RelPath = './components/classes/';

const fetchAll = async (req, res) => {
    try {
        const allClasses = await Classes.find();
        const response = {
            classes: allClasses,
        }
        return res.status(200).json({ response, message: 'All Classes Found' });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error });
    }
}

const addClass = async (req, res) => {
    try {
        const data = req.body;
        const resp = await Classes(data).save();
        return res.status(200).json({ message: 'success', data, resp });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ message: 'error occurred', error: error });
    }
}

const updateClass = async (req, res) => {
    try {
        const data = req.body;
        console.log(req.body);
        return res.status(200).json({ message: 'success', data });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error });
    }
}

const removeClass = async (req, res) => {
    try {
        const data = req.body;
        console.log(req.body);
        return res.status(200).json({ message: 'success', data });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error });
    }
}

const modifyClass = async (req, res) => {
    try {
        const data = req.body;
        console.log(req.body);
        return res.status(200).json({ message: 'success', data });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error });
    }
}

const AClass = async (req, res) => {
    try {
        // const number = base64encode(req.params.number);
        const number = req.params.number;
        const allClasses = await Classes.findOne({ number });
        const response = {
            class: allClasses,
        }
        return res.status(200).json({ message: 'success', response });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const classesView = async (req, res) => {
    try {
        res.render('first', { key: 'value' });

        // const data = { key: 'value' };
        // res.sendFile(__dirname + '/view.html', { data });

        // fs.readFile(`${RelPath}view.html`, function(err, data) {
        //     if (err) {
        //         return res.status(400).json({ message: 'error occurred', error: err });
        //     };
        //     res.writeHead(200, { 'Content-Type': 'text/html' });
        //     res.write(data);
        //     const resp = JSON.stringify({ key: 'value' });
        //     return res.end(resp);
        // });
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

const classesView1 = async (req, res) => {
    try {
        const allClasses = await Classes.find();
        const response = {
            classes: allClasses,
        }
        res.render('classes', response);
    } catch (error) {
        return res.status(400).json({ message: 'error occurred', error: error.message });
    }
}

module.exports = {
    fetchAll,
    addClass,
    removeClass,
    modifyClass,
    updateClass,
    AClass,
    classesView,
    classesView1,
}