const express = require('express');
const router = express.Router();
const baseString = 'class';
const Classes = require('./controller');

// Pug Routes
router.get(`/${baseString}/loadView`, Classes.classesView);
router.get(`/${baseString}/classes-page`, Classes.classesView1);

// API Routes
router.get(`/${baseString}`, Classes.fetchAll);
router.get(`/${baseString}/:number`, Classes.AClass);
router.post(`/${baseString}`, Classes.addClass);
router.put(`/${baseString}`, Classes.updateClass);
router.delete(`/${baseString}`, Classes.removeClass);
router.patch(`/${baseString}`, Classes.modifyClass);

module.exports = router;