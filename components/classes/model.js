const { Schema, model } = require('mongoose');
const classSchema = new Schema({
    number: { type: Number, required: true, unique: true },
},
{ timestamps: true },
);

const Classes = model('classes', classSchema);
module.exports = Classes;
