/*                                            Documentation
Librarian Login
POST: localhost:8080/api/v1/users/login
{
    "login_id":"LIB0008",
    "password":"asdf1234"
}

Update profile
PUT: localhost:8080/api/v1/users/update-profile
Payload form data
first_name:Mr
last_name:Librarian
gender:Male
date_of_birth:1995-04-01
phone_number:112233445

Users List
GET: localhost:8080/api/v1/users/list

Issue book
POST: localhost:8080/api/v1/users/borrow
Payload:
{
    "book_id": ["65126d6e63ee9cba2baa9924", "6511807ad95c626fe1a105a9"],
    "user_id": "65003f5132ff7ff4beee03b1"
}

Add Students Password
PUT: localhost:8080/api/v1/users/set-password/65003bc734f2865c1dbc87bc
{
    "password": "asdf1234"
}

Return Book
PUT: localhost:8080/api/v1/users/return-books
{
    "book_id": ["65126d6e63ee9cba2baa9924", "6511807ad95c626fe1a105a9"],
    "user_id": "65003f5132ff7ff4beee03b1"
}

*/