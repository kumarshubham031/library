const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const app = express();
app.use(logger('dev'));
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser'); // require for form data request
require('dotenv').config(process.env.NODE_ENV);
const server = http.createServer(app);

const Blacklist = require('./components/users/model').Blacklist;

app.use(express.json()); // To Parse JSON bodies
app.use(express.urlencoded({ extended: true })); // TO parse url encoded request bodies (POST Request)

// app.use(bodyParser.json()); // To Parse JSON bodies
// app.use(bodyParser.urlencoded({ extended: true }));

// File Uploading code
app.use(express.static('public'));
app.set('views'), path.join(__dirname, 'views');
app.set('view engine', 'pug');
const uploadFiles = require('./common/upload');
app.post('/upload_me', async (req, res) => {
    uploadFiles.uploadSingle(req, res);
});

app.use(async (req, res, next) => {
    // ! If token is invalidated
    if (req.headers.authorization) {
        const token = await Blacklist.findOne({ token: (req.headers.authorization).split(' ')[1] });
        if (token) {
            return res.status(422).json({ status: false, message: 'Token is invalid.' });
        }
    }
    next();
});
const indexRoute = require('./components/route');
// const viewsRoute = require('./components/route');
app.use('/api/v1', indexRoute);
// app.use('/views', indexRoute);

// ----------------------- Swagger connection start ------------------------------- //
const swaggerUI = require('swagger-ui-express');
const swaggerFile = require('./swagger.json');
app.use('/swagger', swaggerUI.serve, swaggerUI.setup(swaggerFile));
// ----------------------- Swagger connection end   ------------------------------- //
app.get('/chat', (req, res) => {
    res.sendFile(__dirname + '/public/chat.html');
});

app.use('/', (req, res) => res.status(404).json({ status: false, message: 'route not found' }) );
app.use((req, res,next) => {
    next(createError(404));
});

/* const Knex = require('knex');
const { Model } = require('objection');
const connectionObj = {
    client: 'pg',
    useNullAsDefault: true,
    // migration: {
    //     directory: '../../migrations',
    // },
    // seeds: {
    //     directory: '../../seeds',
    // },
    connection: {
        host: 'localhost',
        user: 'postgres',
        password: '',
        database: 'library',
    }
};
(async () => {
    try {
        const knex = Knex(connectionObj);
        Model.knex(knex);
        console.log('db connected successfully');
    } catch (error) {
        console.log('error: ' + error);
    }

}) */

// ----------------------------------- MongoDB Connection Start ----------------------------------- //
const mongoose = require('mongoose');
const MongoString = 'mongodb://localhost:27017/library';
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    family: 4, // To Force IPv4
};
mongoose.set('bufferCommands', true);
const conn = async () => {
    try {
        console.log('DB connected');
        await mongoose.connect(MongoString, options);
    } catch (error) {
        console.log(`error: ${error}`);
    }
};
conn();
// ----------------------------------- MongoDB Connection End ------------------------------------- //

// ---------- Socket ------------- //
const io = require('socket.io')(server);
io.on('connection', (socket) => {
    console.log('socket connected');

    // Send to all
    socket.on('message', (msg) => {
        // console.log(msg);
        socket.broadcast.emit('message', msg)
    })
});
// ---------- Socket ------------- //

server.listen(process.env.PORT, function () {
    console.log('listening on port ' + process.env.PORT);
});