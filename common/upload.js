const path = require('path');
const multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        console.log(file);
        let ext = file.originalname.split('.');
        const name = ext.slice(0, -1);
        ext = ext[ext.length - 1];
        cb (null, name + '_' + Date.now() + '.' +ext);
    }
});
const maxSize = 1 * 1000 * 1000;
var upload = multer({
    storage: storage,
    // limit: { fileSize: maxSize },
    fileFilter: function (req, file, cb) {
        var fileTypes = /jpeg|jpg|png/;
        var mimetype = fileTypes.test(file.mimetype);
        var extname = fileTypes.test(path.extname(file.originalname).toLowerCase());
        if (mimetype && extname) {
            return cb(null, true);
        }
        // cb(JSON.stringify(file))
        cb('Error: file upload only support the type: ' + fileTypes);
    }
});

module.exports = {
    upload,
}