const success = function (res, data, message) {
    return res.status(200).json({ data, message });
}

const failure = function (res, message, statusCode = 400) {
    console.log('error occurred');
    return res.status(statusCode).json({ message });
}

const failure1 = function (res, error, statusCode = 400) {
    const jsonObj = { message: error.message };
    console.log(error.message);
    if (error.stack) { jsonObj.error_in_line = error.stack };
    return res.status(statusCode).json(jsonObj);
}

module.exports = {
    success,
    failure,
    failure1,
}