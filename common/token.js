const jwt = require('jwt-simple');
const moment = require('moment');
const response = require('./response');

const createToken = function (data) {
    const expiry_days = 7;
    let expiry_date = new Date();
    const today = new Date();
    expiry_date = new Date(expiry_date.setDate(expiry_date.getDate() + expiry_days));
    data.created_at = today;
    data.expire_at = expiry_date;
    data.exp = moment().add(expiry_days, 'days').unix();
    return jwt.encode(data, process.env.secret_key);
}

const decodeToken = async (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            return response.failure(res, 'Authorization header missing');
        }
        const decodedToken = (req.headers.authorization).split(' ')[1];
        const decodedData = jwt.decode(decodedToken, process.env.secret_key);
        if (!decodedData) {
            return response.failure(res, 'Invalid Token');
        }
        // console.log(decodedData);
        res.decodedData = decodedData;
        next();
    } catch (error) {
        console.log(error.stack);
        return response.failure(res, error.message);
    }
}

function roleCheck (role = []) {
    return async (req, res, next) => {
        try {
            if (!req.headers.authorization) {
                return response.failure(res, 'Authorization header missing');
            }
            const decodedToken = (req.headers.authorization).split(' ')[1];
            const decodedData = jwt.decode(decodedToken, process.env.secret_key);
            if (!decodedData) {
                return response.failure(res, 'Invalid Token');
            }
            if (!decodedData.role) {
                return response.failure(res, 'Role not found');
            }
            if (role != []) {
                const decodedRole = decodedData.role;
                if (!role.includes(decodedRole)) {
                    return response.failure(res, "You don't have permission to access it");
                }
            }
            res.token_info = decodedData;
            next();
        } catch (error) {
            console.log(error.stack);
            return response.failure(res, error.message);
        }
    }
}

module.exports = {
    createToken,
    decodeToken,
    roleCheck,
}